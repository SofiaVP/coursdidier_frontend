//variables globales

var zoneBodyTableau; //variable qui correspond à la zone du tableau

var tabDevises= []; //tab d'objet js

//permet de démarrer du code dès que la page est téléchargé
window.onload =function(){
    initialiserPage();
}

function initialiserPage(){
    console.log("initialiserPage")
    
    // on fait comme un lien entre l'id de la page html et notre zone 
    //zoneBodyTableau = document.getElementById("bodyTableau");
    zoneBodyTableau = document.querySelector("#bodyTableau");



    tabDevises.push({code:'EUR', nom:'Euro',change: 1})
    tabDevises.push({code:'USD', nom:'Dollar',change: 1.1})

    for(i=0; i<tabDevises.length; i++ ){

        //creer une ligne au tableau
        var nodeTr = document.createElement("tr");
        zoneBodyTableau.appendChild(nodeTr);

        //creer 3 cellules à la ligne qu'on vient d'ajouter au tableau 
        var nodeTd1 = document.createElement("td");
        nodeTr.appendChild(nodeTd1); nodeTd1.innerHTML=tabDevises[i].code;
        var nodeTd2 = document.createElement("td");
        nodeTr.appendChild(nodeTd2); nodeTd2.innerHTML=tabDevises[i].nom;
        var nodeTd3 = document.createElement("td");
        nodeTr.appendChild(nodeTd3); nodeTd3.innerHTML=tabDevises[i].change;
    }  

}

function ajoutDevises(){

    //recuperer le contenu des zones saisies (code, nom, change)
    var valCode = document.getElementById("code").value;
    var valNom = document.getElementById("nom").value;
    var valChange = document.getElementById("change").value;

    var nouvelleDevise = {
        code : valCode, 
        nom : valNom, 
        change : valChange
       }
    
    //ajout de nouvelleDevise dans le tableau javascript tabDevises -> tab invisible

    tabDevises.push(nouvelleDevise);

    //ajout de nouvelleDevise dans le tableau HTML(partie zoneBodyTableau) -> tab visible 
        var i = tabDevises.length-1; //pour ajouter à la fin du tableau
        console.log(i);

        //creer une ligne au tableau
        var nodeTr = document.createElement("tr");
        zoneBodyTableau.appendChild(nodeTr);

        //creer 3 cellules à la ligne qu'on vient d'ajouter au tableau 
        var nodeTd1 = document.createElement("td");
        nodeTr.appendChild(nodeTd1); nodeTd1.innerHTML=tabDevises[i].code;
        var nodeTd2 = document.createElement("td");
        nodeTr.appendChild(nodeTd2); nodeTd2.innerHTML=tabDevises[i].nom;
        var nodeTd3 = document.createElement("td");
        nodeTr.appendChild(nodeTd3); nodeTd3.innerHTML=tabDevises[i].change;
      

}